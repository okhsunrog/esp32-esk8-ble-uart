# esp32-esk8-ble-uart

A simple VESC UART <-> BLE bridge for using with VESC Tool.
You only need a esp32 and a PlatformIO installed. The code uses Arduino framework and NimBLE lib

## Wiring

| ESP32               | VESC        |
|---------------------|-------------|
| GND                 | GND         |
| 3.3v                | VCC (3.3v)  |
| GPIO17 (UART2 TX)   | RX          |
| GPIO16 (UART2 RX)   | TX          |

