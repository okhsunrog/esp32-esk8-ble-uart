#include <Arduino.h>
#include <NimBLEDevice.h>

#define FULL_PACKET 512

int MTU_SIZE = FULL_PACKET;
int BLE_PACKET_SIZE = MTU_SIZE - 3;

std::string bufferString;

#define VESC_SERVICE_UUID            "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define VESC_CHARACTERISTIC_UUID_RX  "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define VESC_CHARACTERISTIC_UUID_TX  "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

NimBLECharacteristic* vesc_char_tx = NULL;
NimBLECharacteristic* vesc_char_rx = NULL;
NimBLEServer *pServer = NULL;


class MyCallbacks: public NimBLECharacteristicCallbacks {
    void onWrite(NimBLECharacteristic* pCharacteristic) {
        std::string rxValue = pCharacteristic->getValue();
        if (rxValue.length() > 0){
            if (pCharacteristic->getUUID().equals(vesc_char_rx->getUUID())){
                for (int i = 0; i < rxValue.length(); i++) {
                    Serial2.write(rxValue[i]);
                }
            }
        }
    };

    void onMTUChange(uint16_t MTU, ble_gap_conn_desc* desc) {
        MTU_SIZE = MTU;
        BLE_PACKET_SIZE = MTU_SIZE - 3;
    }
};


void setup()
{
    Serial2.begin(115200, SERIAL_8N1, 16, 17);

    while (!Serial2) {};

    NimBLEDevice::init("VESC BLE");
    NimBLEDevice::setMTU(MTU_SIZE);
    pServer = NimBLEDevice::createServer();
    NimBLEService *vescService = pServer->createService(VESC_SERVICE_UUID);
    vesc_char_tx = vescService->createCharacteristic(VESC_CHARACTERISTIC_UUID_TX,
                                                                NIMBLE_PROPERTY::NOTIFY |
                                                                NIMBLE_PROPERTY::READ);
    vesc_char_rx = vescService->createCharacteristic(VESC_CHARACTERISTIC_UUID_RX,
                                                                NIMBLE_PROPERTY::WRITE);

    vesc_char_rx->setCallbacks(new MyCallbacks);
    vescService->start();
    pServer->advertiseOnDisconnect(true);
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(VESC_SERVICE_UUID);
    pAdvertising->setAppearance(0x00);
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x0); 
    pAdvertising->start();    
}


void loop() {
    if(Serial2.available()){
        int oneByte;
        while(Serial2.available()){
            oneByte = Serial2.read();
            bufferString.push_back(oneByte);
        }
        if (pServer->getConnectedCount()) {
            while (bufferString.length() > 0) {
                if (bufferString.length() > BLE_PACKET_SIZE) {
                    vesc_char_tx->setValue(bufferString.substr(0, BLE_PACKET_SIZE));
                    bufferString = bufferString.substr(BLE_PACKET_SIZE);
                } else {
                    vesc_char_tx->setValue(bufferString);
                    bufferString.clear();
                }
                vesc_char_tx->notify();
                delay(10); // bluetooth stack will go into congestion, if too many packets are sent
            }
        }
    }
}